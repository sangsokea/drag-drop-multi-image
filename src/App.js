import { useDropzone } from "react-dropzone";

function App() {
  const { acceptedFiles, getRootProps, getInputProps } = useDropzone({
    multiple: true,
  });

  // display files
  const files = acceptedFiles.map((file) => {
    console.log(file);
    return (
      <li key={file.path}>
        <figure class="max-w-lg">
          <img
            class="h-auto max-w-full rounded-lg"
            src={URL.createObjectURL(file)}
            alt="image description"
          />
          <figcaption class="mt-2 text-sm text-center text-gray-500 dark:text-gray-400">
            {file.path} - {file.size} bytes
          </figcaption>
        </figure>
      </li>
    );
  });

  console.log(files);
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <section className="flex flex-col items-center justify-center">
        <h1 className="text-4xl font-bold text-center">
          React Dropzone a File
        </h1>
        <div {...getRootProps({ className: "dropzone" })}>
          <input {...getInputProps()} />
          <div className="m-9 p-9 border-dashed border-2 border-indigo-600">
            <p>Drag 'n' drop some files here, or click to select files</p>
          </div>
        </div>
        <aside>
          <h4 className="text-xl font-bold">Files</h4>
          <ul>{files}</ul>
        </aside>
      </section>
    </main>
  );
}

export default App;
